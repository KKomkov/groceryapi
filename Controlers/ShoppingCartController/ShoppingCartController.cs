﻿using AutoMapper;
using Grocery.Interfaces.Services;
using Grocery.Models.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace ShoppingCartController
{
    /// <summary>
    /// Provide operations for Shopping cart
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class ShoppingCartController : ControllerBase
    {
        protected IPurchaseService PurchaseService { get; }
        protected IOrderService OrderService { get; }
        protected IMapper _mapper;

        public ShoppingCartController(IPurchaseService purchaseService, IOrderService orderService, IMapper mapper)
        {
            PurchaseService = purchaseService;
            OrderService = orderService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get shopping cart content; Always return not null instance;
        /// </summary>
        /// <param name="userID">User identificator; In future will be get from user token.</param>
        /// <returns>ShoppingCart</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ShoppingCart>), StatusCodes.Status200OK)]
        public async Task<ActionResult<ShoppingCart>> GetShoppingCart([FromQuery]int userID)
        {
            var cartDTO = await PurchaseService.GetShoppingCartAsync(userID);

            ShoppingCart cart = _mapper.Map<ShoppingCart>(cartDTO);

            return Ok(cart);
        }

        /// <summary>
        /// Add product into cart with desirable quantity.
        /// </summary>
        /// <param name="userID">User identificator; In future will be get from user token.</param>
        /// <param name="productID">Product id</param>
        /// <param name="quantity">Product amount that will be added into cart</param>
        /// <returns>ShoppingCart</returns>
        [HttpPost("Product/{productID:int}")]
        [ProducesResponseType(typeof(IEnumerable<ShoppingCart>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(IList<string>), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ShoppingCart>> AddProduct(
                                                                 [FromQuery] int userID,
                                                                 [FromRoute][Range(1, int.MaxValue)] int productID,
                                                                 [FromQuery][Range(1, int.MaxValue)] int quantity = 1)
        {
            var response = await PurchaseService.AddProductAsync(userID, productID, quantity);

            if (response.IsSuccesfull)
                return await GetShoppingCart(userID);
            else
                return BadRequest(response.Errors);
        }

        /// <summary>
        /// Set product quantity in Shopping Cart
        /// </summary>
        /// <param name="userID">User identificator; In future will be get from user token.</param>
        /// <param name="productID">Product id</param>
        /// <param name="quantity">Required amount  of a product in a cart</param>
        /// <returns>ShoppingCart</returns>
        [HttpPut("Product/{productID:int}")]
        [ProducesResponseType(typeof(IEnumerable<ShoppingCart>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(IList<string>), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ShoppingCart>> EditProductQuantity(
                                                                 [FromQuery] int userID,
                                                                 [FromRoute][Range(1, int.MaxValue)] int productID,
                                                                 [FromQuery][Range(1, int.MaxValue)] int quantity = 1)
        {
            var response = await PurchaseService.SetProductAmountAsync(userID, productID, quantity);

            if (response.IsSuccesfull)
                return await GetShoppingCart(userID);
            else
                return BadRequest(response.Errors);
        }

        /// <summary>
        /// Delete a product from shopping cart.
        /// </summary>
        /// <param name="userID">User identificator; In future will be get from user token.</param>
        /// <param name="productID">Product id</param>
        /// <returns>ShoppingCart</returns>
        [HttpDelete("Product/{productID:int}")]
        [ProducesResponseType(typeof(IEnumerable<ShoppingCart>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(IList<string>), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ShoppingCart>> DeleteProduct(
                                                                 [FromQuery] int userID,
                                                                 [FromRoute][Range(1, int.MaxValue)] int productID)
        {
            var response = await PurchaseService.DeleteProductAsync(userID, productID);

            if (response.IsSuccesfull)
                return await GetShoppingCart(userID);
            else
                return BadRequest(response.Errors);
        }

        [HttpPost("Checkout")]
        [ProducesResponseType(typeof(OrderBase), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ICollection<ProductError>), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CheckoutShoppingCart([FromQuery] int userID)
        {
            var result = await OrderService.CheckoutCartAsync(userID);

            if (result.Errors?.Count > 0)
            {
                ICollection<ProductError> errors = _mapper.Map<ICollection<ProductError>>(result.Errors);
                return BadRequest(errors);
            }
            else
            {
                OrderBase newOrder = _mapper.Map<OrderBase>(result.NewOrder);
                return RedirectToRoute(new
                {
                    controller = "Orders",
                    action = "Get",
                    userId = userID,
                    orderID = newOrder.OrderID
                });
                //return Ok(newOrder);
            }
        }
    }
}