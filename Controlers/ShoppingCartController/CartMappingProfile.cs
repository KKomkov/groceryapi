﻿using AutoMapper;
using Grocery.Models.DTO;
using Grocery.Models.Response;

namespace ShoppingCartController
{
    /// <summary>
    /// Define mapping profile for Automapper
    /// </summary>
    public class CartMappingProfile : Profile
    {
        public CartMappingProfile()
        {
            CreateMap<ProductBaseDTO, PurchaseItem>();

            CreateMap<PurchaseItemDTO, PurchaseItem>().IncludeMembers(src => src.Item);

            CreateMap<ShoppingCartDTO, ShoppingCart>();

            CreateMap<ProductErrorDTO, ProductError>();

            CreateMap<OrderBaseDTO, OrderBase>();
        }
    }
}