﻿using AutoMapper;
using Grocery.Interfaces.Services;
using Grocery.Models.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace ProductsController
{
    /// <summary>
    /// Provide operation for Product data
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        protected IProductService ProductService { get; }
        protected IMapper _mapper;

        public ProductsController(IProductService productService, IMapper mapper)
        {
            ProductService = productService;
            _mapper = mapper;
        }

        //TODO:add method to show products out of scope

        /// <summary>
        /// Get all available product information.
        /// </summary>
        /// <returns>IEnumerable(Product)</returns>
        [HttpGet("")]
        [ProducesResponseType(typeof(IEnumerable<Product>), StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Product>>> Get()
        {
            var products = await ProductService.GetAllProductsAsync();
            var resultProducts = _mapper.Map<IEnumerable<Product>>(products);

            return Ok(resultProducts);
        }

        /// <summary>
        /// Get product information correspond to given productID.
        /// </summary>
        /// <param name="id">Product unique id</param>
        /// <returns>Product data</returns>
        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(Product), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Product>> Get([Range(1, int.MaxValue)] int id)
        {
            var product = await ProductService.GetProductAsync(id);
            if (product != null)
            {
                var resultProducts = _mapper.Map<Product>(product);
                return Ok(resultProducts);
            }
            else
                return NotFound();
        }
    }
}