﻿using AutoMapper;
using Grocery.Models.DTO;
using Grocery.Models.Response;

namespace ProductsController
{
    public class ProductMappingProfile : Profile
    {
        public ProductMappingProfile()
        {
            CreateMap<ProductDTO, Product>();
        }
    }
}