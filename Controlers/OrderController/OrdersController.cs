﻿using AutoMapper;
using Grocery.Interfaces.Services;
using Grocery.Models.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace OrderController
{
    /// <summary>
    ///
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class OrdersController : ControllerBase
    {
        protected IOrderService OrderService { get; }
        protected IMapper _mapper;

        public OrdersController(IOrderService orderService, IMapper mapper)
        {
            OrderService = orderService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all available user Orders.
        /// </summary>
        /// <param name="userID">User unique id</param>
        /// <returns>collection of Orders</returns>
        [HttpGet("")]
        [ProducesResponseType(typeof(IEnumerable<Order>), StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Order>>> Get([FromQuery] int userID)
        {
            var Orders = await OrderService.GetAllOrdersAsync(userID);
            var resultOrders = _mapper.Map<IEnumerable<Order>>(Orders);

            return Ok(resultOrders);
        }

        /// <summary>
        /// Get Order information correspond to given OrderID.++++++
        /// </summary>
        /// <param name="userID">User unique id</param>
        /// <param name="orderID">Order unique id</param>
        /// <returns>Order data</returns>
        [HttpGet("{orderID:int}")]
        [ProducesResponseType(typeof(Order), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Order>> Get([FromQuery] int userID,
                                                   [Range(1, int.MaxValue)] int orderID)
        {
            var Order = await OrderService.GetOrderAsync(userID, orderID);
            if (Order != null)
            {
                var resultOrders = _mapper.Map<Order>(Order);
                return Ok(resultOrders);
            }
            else
                return NotFound();
        }
    }
}