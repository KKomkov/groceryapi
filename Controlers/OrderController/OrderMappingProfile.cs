﻿using AutoMapper;
using Grocery.Models.DTO;
using Grocery.Models.Response;

namespace OrderController
{
    /// <summary>
    /// Define mapping profile for Automapper
    /// </summary>
    public class OrderMappingProfile : Profile
    {
        public OrderMappingProfile()
        {
            CreateMap<OrderBaseDTO, OrderBase>();

            CreateMap<ProductBaseDTO, OrderItem>();
            CreateMap<OrderItemDTO, OrderItem>()
                .IncludeMembers(x => x.Product);
            CreateMap<OrderDTO, OrderBase>();
            CreateMap<OrderDTO, Order>()
                .ForMember(dest => dest.Products, cf => cf.MapFrom(src => src.OrderItems));
        }
    }
}