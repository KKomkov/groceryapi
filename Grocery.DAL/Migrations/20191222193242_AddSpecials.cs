﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Grocery.DAL.Migrations
{
    public partial class AddSpecials : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Specials",
                columns: new[] { "SpecialItemID", "BonusPrice", "BonusProductID", "BonusQuantity", "MinQuantity", "SourceProductID" },
                values: new object[] { 1, 0m, 3, 1, 2, 3 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Specials",
                keyColumn: "SpecialItemID",
                keyValue: 1);
        }
    }
}