﻿using AutoMapper;
using Grocery.DAL.Models;
using Grocery.Models.DTO;

namespace Grocery.DAL
{
    public class DbEntityMappingProfile : Profile
    {
        public DbEntityMappingProfile()
        {
            CreateMap<Product, ProductBaseDTO>();

            CreateMap<Product, ProductDTO>()
                .ForMember(dest => dest.Quantity, cf => cf.MapFrom(src => src.ProductQuantity.Quantity));

            CreateMap<PurchaseItem, PurchaseItemDTO>()
             .ForMember(dest => dest.Item, cf => cf.MapFrom(src => src.Product));

            CreateMap<PurchaseItemCreateDTO, PurchaseItem>();
            CreateMap<PurchaseBonusItemCreateDTO, PurchaseItem>();

            CreateMap<Special, BonusDTO>();

            CreateMap<PurchaseItem, OrderItem>()
                .ForMember(src => src.OrderId, cf => cf.Ignore())
                .ForMember(src => src.OrderItemId, cf => cf.Ignore());

            CreateMap<OrderItemCreateDTO, OrderItem>();
            CreateMap<OrderItem, OrderItemDTO>();
            CreateMap<Order, OrderBaseDTO>();
            CreateMap<Order, OrderDTO>();
            CreateMap<OrderCreateDTO, Order>();

            CreateMap<ProductQuantity, ProductQuantityDTO>();

            CreateMap<PurchaseItemDTO, OrderItemCreateDTO>()
                .ForMember(dest => dest.ProductId, cf => cf.MapFrom(src => src.Item.ProductID));
        }
    }
}