﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Grocery.DAL.Data;
using Grocery.DAL.Models;
using Grocery.Interfaces.DAL;
using Grocery.Models.DTO;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grocery.DAL
{
    public class PurchaseDAL : BaseDAL, IPurchaseDAL
    {
        protected IMapper _mapper;

        public PurchaseDAL(GroceryDbContext dbContext, IMapper mapper)
        {
            SetDBContext(dbContext);
            _mapper = mapper;
        }

        public async Task<List<PurchaseItemDTO>> GetAllPurchasedItemsAsync(int userId)
        {
            var purchasedItems = await DbContext.PurchaseItems
                                  .Where(x => x.UserId.Equals(userId)
                                         )
                                  .ProjectTo<PurchaseItemDTO>(_mapper.ConfigurationProvider)
                                  .ToListAsync();

            return purchasedItems;
        }

        public async Task<List<ProductQuantityDTO>> GetPurchasedItemsStockAsync(int userID)
        {
            var purchasedItemsStock = await DbContext.PurchaseItems
                                  .Where(x => x.UserId.Equals(userID)
                                         )
                                  .Join(DbContext.ProductQuantities,
                                   item => item.ProductId,
                                   stock => stock.ProductId,
                                   (item, stock) => stock
                                  )
                                  .Distinct()
                                  .ProjectTo<ProductQuantityDTO>(_mapper.ConfigurationProvider)
                                  .ToListAsync();
            return purchasedItemsStock;
        }

        public async Task<List<int>> GetPurchasedItemsOutOfStockAsync(int userID)
        {
            List<int> result = null;

            var purchasedProductGrupped = DbContext.PurchaseItems
                                  .Where(x => x.UserId.Equals(userID)
                                         )
                                  .GroupBy(x => x.ProductId)
                                  .Select(g => new
                                  {
                                      ProductID = g.Key,
                                      Quantity = g.Sum(x => x.Quantity),
                                      TotalPrice = g.Sum(x => x.ItemPrice)
                                  }).AsQueryable();

            result = await purchasedProductGrupped.Join(DbContext.ProductQuantities,
                                         item => item.ProductID,
                                         stock => stock.ProductId,
                                         (item, stock) => new
                                         {
                                             ProductID = item.ProductID,
                                             Quantity = item.Quantity,
                                             StockQuantity = stock.Quantity
                                         }
                                         )
                                        .Where(x => x.Quantity > x.StockQuantity)
                                        .Select(x => x.ProductID)
                                        .ToListAsync();

            return result;
        }

        public async Task<List<PurchaseItemDTO>> GetProductPurchasedItemsAsync(int userId, int productID, bool includeSpecials = false)
        {
            var purchasedItems = await DbContext.PurchaseItems
                                 .Where(x => x.UserId.Equals(userId)
                                            && x.ProductId == productID
                                            && (includeSpecials || x.IsSpecials == false))
                                 .ProjectTo<PurchaseItemDTO>(_mapper.ConfigurationProvider)
                                 .ToListAsync();

            return purchasedItems;
        }

        public async Task AddProductToCartAsync(PurchaseItemCreateDTO newItem)
        {
            //We preserve cases when product price was changed while product exists in PurchaseItems
            //Such cases should be specified separately

            var productItem = await DbContext.Products.FindAsync(newItem.ProductID);

            var row = await DbContext.PurchaseItems
                                    .Where(x => x.UserId == newItem.UserID
                                        && x.ProductId == newItem.ProductID)
                                    .FirstOrDefaultAsync();

            newItem.ItemPrice = productItem.Price ?? 0;

            if (row != null)
            {
                row.Quantity += newItem.Quantity;
            }
            else
            {
                row = _mapper.Map<PurchaseItem>(newItem);

                DbContext.PurchaseItems.Add(row);
            }

            await DbContext.SaveChangesAsync();
        }

        public async Task DeleteAllProductsFromCartAsync(int userID)
        {
            var items = DbContext.PurchaseItems
                             .Where(x => x.UserId == userID);

            DbContext.PurchaseItems.RemoveRange(items);
            await DbContext.SaveChangesAsync();
        }

        public async Task<bool> DeleteProductFromCartAsync(int userID, int productID)
        {
            var entity = await DbContext.PurchaseItems
                             .Where(x => x.UserId == userID && x.ProductId == productID)
                             .FirstOrDefaultAsync();

            bool rowFound = (entity != null);
            if (rowFound)
            {
                DbContext.PurchaseItems.Remove(entity);
                await DbContext.SaveChangesAsync();
            }

            return rowFound;
        }

        public async Task<bool> SetProductQuantityAsync(int userID, int productID, int quantity)
        {
            var entity = await DbContext.PurchaseItems
                             .Where(x => x.UserId == userID && x.ProductId == productID)
                             .FirstOrDefaultAsync();

            bool rowFound = (entity != null);
            if (rowFound)
            {
                entity.Quantity = quantity;
                await DbContext.SaveChangesAsync();
            }

            return rowFound;
        }

        public async Task<List<BonusDTO>> GetProductRelatedBonusesAsync(int productID)
        {
            var bonusItems = await DbContext.Specials
                                    .Where(x => x.SourceProductId == productID)
                                    .ProjectTo<BonusDTO>(_mapper.ConfigurationProvider)
                                    .ToListAsync();

            return bonusItems;
        }

        public async Task AddBonusesToCartAsync(IEnumerable<PurchaseBonusItemCreateDTO> bonusItems)
        {
            var newItems = _mapper.Map<IEnumerable<PurchaseItem>>(bonusItems);

            DbContext.PurchaseItems.AddRange(newItems);

            await DbContext.SaveChangesAsync();
        }

        public async Task DeleteProductBonusesAsync(int userID, int productID)
        {
            var bonuses = DbContext.PurchaseItems
                            .Where(x => x.UserId == userID && x.SourceProductId == productID);

            DbContext.PurchaseItems.RemoveRange(bonuses);
            await DbContext.SaveChangesAsync();
        }
    }
}