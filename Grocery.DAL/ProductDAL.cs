﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Grocery.DAL.Data;
using Grocery.Interfaces.DAL;
using Grocery.Models.DTO;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grocery.DAL
{
    public class ProductDAL : BaseDAL, IProductDAL
    {
        protected IMapper _mapper;

        public ProductDAL(GroceryDbContext dbContext, IMapper mapper)
        {
            SetDBContext(dbContext);
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProductDTO>> GetAllProductAsync(bool showOutOfStock = false)
        {
            var result = await DbContext.Products
                                .Where(x => showOutOfStock || (x.ProductQuantity.Quantity > 0))
                                .ProjectTo<ProductDTO>(_mapper.ConfigurationProvider)
                                .ToListAsync();

            return result;
        }

        public async Task<ProductDTO> GetProductAsync(int productId)
        {
            ProductDTO result = null;

            var dbProductItem = await DbContext.Products
                               .FindAsync(productId);

            if (dbProductItem != null)
                result = _mapper.Map<ProductDTO>(dbProductItem);

            return result;
        }

        public async Task<bool> IsProductInStockAsync(int productId, int quantity = 1)
        {
            var result = await DbContext.ProductQuantities
                                .AnyAsync(x => x.ProductId == productId
                                            && x.Quantity >= quantity
                                         );

            return result;
        }
    }
}