﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Grocery.DAL.Models
{
    [Table("PurchaseItem")]
    public partial class PurchaseItem
    {
        [Key]
        [Column("PurchaseItemID")]
        public int PurchaseItemId { get; set; }
        [Column("UserID")]
        public int UserId { get; set; }
        [Column("ProductID")]
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal ItemPrice { get; set; }
        public bool IsSpecials { get; set; }
        [Column("SourceProductID")]
        public int? SourceProductId { get; set; }

        [ForeignKey(nameof(ProductId))]
        [InverseProperty("PurchaseItemProducts")]
        public virtual Product Product { get; set; }
        
        [ForeignKey(nameof(SourceProductId))]        
        public virtual Product SourceProduct { get; set; }
    }
}