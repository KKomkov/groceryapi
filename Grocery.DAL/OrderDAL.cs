﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Grocery.DAL.Data;
using Grocery.DAL.Models;
using Grocery.Interfaces.DAL;
using Grocery.Models.DTO;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grocery.DAL
{
    public class OrderDAL : BaseDAL, IOrderDAL
    {
        protected IMapper _mapper;

        public OrderDAL(GroceryDbContext dbContext, IMapper mapper)
        {
            SetDBContext(dbContext);
            _mapper = mapper;
        }

        public async Task<OrderBaseDTO> CreateOrderAsync(OrderCreateDTO newOrder)
        {
            var order = _mapper.Map<Order>(newOrder);
            DbContext.Orders.Add(order);

            await DbContext.SaveChangesAsync();

            var result = _mapper.Map<OrderBaseDTO>(order);
            return result;
        }

        public async Task<IEnumerable<OrderDTO>> GetAllOrdersAsync(int userID)
        {
            var result = await DbContext.Orders
                                .Include(order => order.OrderItems)
                                .ThenInclude(x => x.Product)
                                .Where(x => x.UserId == userID)
                                .ProjectTo<OrderDTO>(_mapper.ConfigurationProvider)
                                .ToListAsync();

            return result;
        }

        public async Task<OrderDTO> GetOrderAsync(int userID, int orderID)
        {
            var row = await DbContext.Orders
                                .Include(order => order.OrderItems)
                                .ThenInclude(x => x.Product)
                                .Where(x => x.OrderId == orderID && x.UserId == userID)
                                .FirstOrDefaultAsync();

            OrderDTO result = _mapper.Map<OrderDTO>(row);

            return result;
        }
    }
}