﻿using Grocery.DAL.Data;
using Grocery.Interfaces.DAL;
using Microsoft.EntityFrameworkCore.Storage;
using System.Threading.Tasks;

namespace Grocery.DAL
{
    public class BaseDAL : IBaseDAL
    {
        protected GroceryDbContext DbContext { get; private set; }

        public void SetDBContext(GroceryDbContext dbContext)
        {
            DbContext = dbContext;
        }

        protected IDbContextTransaction _transaction;

        public async Task BeginTransactionAsync()
        {
            _transaction = await DbContext.Database.BeginTransactionAsync();
        }

        public async Task CommitTransactionAsync()
        {
            await _transaction.CommitAsync();
            _transaction.Dispose();
        }

        public async Task RollbackTransactionAsync()
        {
            await _transaction.RollbackAsync();
            _transaction.Dispose();
        }
    }
}