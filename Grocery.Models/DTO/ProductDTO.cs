﻿namespace Grocery.Models.DTO
{
    public class ProductDTO : ProductBaseDTO
    {
        public int Quantity { get; set; }
    }
}