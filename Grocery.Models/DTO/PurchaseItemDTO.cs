﻿namespace Grocery.Models.DTO
{
    public class PurchaseItemDTO
    {
        public ProductBaseDTO Item { get; set; }
        public int Quantity { get; set; }
        public bool IsSpecials { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
}