﻿namespace Grocery.Models.DTO
{
    public class OrderItemDTO
    {
        public ProductBaseDTO Product { get; set; }
        public int Quantity { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
}