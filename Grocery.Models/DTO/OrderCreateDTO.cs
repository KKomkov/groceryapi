﻿using Grocery.Models.BO.Enums;
using System;
using System.Collections.Generic;

namespace Grocery.Models.DTO
{
    public class OrderDTO : OrderBaseDTO
    {
        public OrderStatus Status { get; set; }
        public List<OrderItemDTO> OrderItems { get; set; }

        public decimal PurchaseTotal { get; set; }
        public DateTime PurchaseDate { get; set; }
    }
}