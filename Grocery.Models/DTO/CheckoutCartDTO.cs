﻿using System.Collections.Generic;

namespace Grocery.Models.DTO
{
    public class CheckoutCartDTO
    {
        public ICollection<ProductErrorDTO> Errors { get; set; }
        public OrderBaseDTO NewOrder { get; set; }
    }
}