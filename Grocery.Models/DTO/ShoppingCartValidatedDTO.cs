﻿using System.Collections.Generic;

namespace Grocery.Models.DTO
{
    public class ShoppingCartValidatedDTO : ShoppingCartDTO
    {
        public ICollection<ProductErrorDTO> Errors { get; set; }
    }
}