﻿using System.Collections.Generic;

namespace Grocery.Models.DTO
{
    public class ShoppingCartDTO
    {
        public IList<PurchaseItemDTO> Items { get; set; }

        public decimal TotalPrice { get; set; }
    }
}