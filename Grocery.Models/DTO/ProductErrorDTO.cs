﻿namespace Grocery.Models.DTO
{
    public class ProductErrorDTO
    {
        public int ProductID { get; set; }
        public string ErrorMessage { get; set; }
    }
}