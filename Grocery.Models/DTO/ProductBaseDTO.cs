﻿using System;

namespace Grocery.Models.DTO
{
    public class ProductBaseDTO
    {
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Uri Photo { get; set; }
        public decimal Price { get; set; }
    }
}