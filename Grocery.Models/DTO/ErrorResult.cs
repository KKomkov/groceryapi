﻿using System.Collections.Generic;

namespace Grocery.Models.DTO
{
    public class OperationResult
    {
        public OperationResult(bool isSuccesfull)
        {
            IsSuccesfull = isSuccesfull;
            if (!isSuccesfull)
                Errors = new List<string>();
        }

        public OperationResult(string errorMessage)
        {
            IsSuccesfull = false;
            Errors = new List<string>() { errorMessage };
        }

        public bool IsSuccesfull { get; set; }
        public IList<string> Errors { get; }
    }
}