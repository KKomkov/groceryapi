﻿using Grocery.Models.BO.Enums;
using System;
using System.Collections.Generic;

namespace Grocery.Models.DTO
{
    public class OrderCreateDTO
    {
        public OrderStatus Status { get; set; }
        public List<OrderItemCreateDTO> OrderItems { get; set; }

        public decimal PurchaseTotal { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int UserId { get; set; }
    }
}