﻿namespace Grocery.Models.DTO
{
    public class PurchaseItemCreateDTO
    {
        public int UserID { get; set; }
        public int ProductID { get; set; }
        public int Quantity { get; set; }

        public decimal ItemPrice { get; set; }
    }

    public class PurchaseBonusItemCreateDTO : PurchaseItemCreateDTO
    {
        public bool IsSpecials { get; set; } = true;

        public int? SourceProductID { get; set; }
    }
}