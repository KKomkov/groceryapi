﻿namespace Grocery.Models.DTO
{
    public class BonusDTO
    {
        public int SourceProductId { get; set; }
        public int MinQuantity { get; set; }

        public int BonusProductId { get; set; }
        public int BonusQuantity { get; set; }
        public decimal BonusPrice { get; set; }
    }
}