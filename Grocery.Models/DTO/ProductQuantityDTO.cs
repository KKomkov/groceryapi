﻿namespace Grocery.Models.DTO
{
    public class ProductQuantityDTO
    {
        public int ProductID { get; set; }
        public int Quantity { get; set; }
    }
}