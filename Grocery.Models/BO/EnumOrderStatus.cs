﻿namespace Grocery.Models.BO.Enums
{
    public enum OrderStatus
    {
        Created = 1,
        Collecting = 2,
        Completed = 3
    }
}