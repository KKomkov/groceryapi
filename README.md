# README #
Grocery web api implementation.

How to run the app: 
1 Unpack source app.
2 Set connection string to your database:
	Edit Grocery.Main\appsettings.json and set connection to your database
	At this moment database could be not created yet.
3 Apply database migration script 
	Way 1 using command line:
		3.1 In command line navigate to solution folder 
		3.2 excute command 
			dotnet ef database update -p Grocery.DAL  -s Grocery.Main
	Way 2 with VS 
		3.1 Open solution Grocery.sln
		3.2 In Package management console run Update-Database
	Way 3 in case ef Core migration doesnot works
		 Restore mssql backup from file 
		 ..\GroceryDb2.bak 
4 Run application: 
	In command line navigate to the solution folder and execute command 
	dotnet run  --project Grocery.Main
	
	By default application could be available by the url http://localhost:5000
	Swagger available on http://localhost:5000/swagger

	
5 Postman collection available by the public link
	https://documenter.getpostman.com/view/2555851/SWLYBAfr
	also you may import collection from swagger file
	../swaggerExportedGrocery API.json  in archive
	the same file could be reached by link  
	api url+ /swagger/v1/swagger.json