using AutoMapper;
using Grocery.DAL;
using Grocery.DAL.Data;
using Grocery.Interfaces.DAL;
using Grocery.Interfaces.Services;
using Grocery.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Grocery.Main
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Grocery.DAL.DbEntityMappingProfile),
                                   typeof(ProductsController.ProductMappingProfile),
                                   typeof(ShoppingCartController.CartMappingProfile),
                                   typeof(OrderController.OrderMappingProfile)
                                   );

            services.AddControllers()
                    .AddJsonOptions(options =>
                    {
                        options.JsonSerializerOptions.IgnoreNullValues = true;
                        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter(JsonNamingPolicy.CamelCase));
                    });
            services.AddMvcCore()
                    .AddApiExplorer();

            services.AddDbContext<GroceryDbContext>(options =>
            {
                options.UseSqlServer(Configuration["ConnectionStrings:DefaultConnection"]);
            });

            services.AddTransient<IProductDAL, ProductDAL>();
            services.AddTransient<IPurchaseDAL, PurchaseDAL>();
            services.AddTransient<IOrderDAL, OrderDAL>();

            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IPurchaseService, PurchaseService>();
            services.AddTransient<IOrderService, OrderService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "Grocery API", Version = "version 1" });

                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                //add methods summary into swagger
                //there possible some optimization in case of huge  controllers amount.
                // c.IncludeXmlComments(Path.Combine(basePath, "Grocery.Main.xml"));
                c.IncludeXmlComments(Path.Combine(basePath, "ProductsController.xml"));
                c.IncludeXmlComments(Path.Combine(basePath, "ShoppingCartController.xml"));
                c.IncludeXmlComments(Path.Combine(basePath, "OrderController.xml"));
            });
            services.AddSwaggerGenNewtonsoftSupport();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
        }
    }
}