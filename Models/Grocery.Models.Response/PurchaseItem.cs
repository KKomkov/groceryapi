﻿using System;

namespace Grocery.Models.Response
{
    public class PurchaseItem
    {
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Uri Photo { get; set; }
        public decimal ItemPrice { get; set; }
        public int Quantity { get; set; }
        public bool IsSpecials { get; set; }
        public decimal TotalPrice { get; set; }
    }
}