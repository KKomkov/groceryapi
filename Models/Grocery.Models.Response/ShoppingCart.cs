﻿using System.Collections.Generic;

namespace Grocery.Models.Response
{
    public class ShoppingCart
    {
        public ICollection<PurchaseItem> Items { get; set; }

        public decimal TotalPrice { get; set; }
    }

    public class ShoppingCartValidated : ShoppingCart
    {
        /// <summary>
        /// Represent purchased item checkout errors
        /// </summary>
        public ICollection<ProductError> Errors { get; set; }
    }
}