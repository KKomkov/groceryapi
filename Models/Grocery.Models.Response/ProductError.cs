﻿namespace Grocery.Models.Response
{
    public class ProductError
    {
        public int ProductID { get; set; }
        public string ErrorMessage { get; set; }
    }
}