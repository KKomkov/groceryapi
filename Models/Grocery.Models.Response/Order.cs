﻿using Grocery.Models.BO.Enums;
using System;
using System.Collections.Generic;

namespace Grocery.Models.Response
{
    public class Order
    {
        public int OrderID { get; set; }
        public OrderStatus Status { get; set; }
        public List<OrderItem> Products { get; set; }
        public decimal PurchaseTotal { get; set; }
        public DateTime PurchaseDate { get; set; }
    }
}