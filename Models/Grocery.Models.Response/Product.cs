﻿using System;

namespace Grocery.Models.Response
{
    public class Product
    {
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Uri Photo { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }
}