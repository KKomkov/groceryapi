﻿using Grocery.Interfaces.DAL;
using Grocery.Interfaces.Services;
using Grocery.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grocery.Services
{
    public class PurchaseService : IPurchaseService
    {
        protected IPurchaseDAL CartDAL { get; }
        protected IProductDAL ProductDAL { get; }

        public PurchaseService(IPurchaseDAL purchaseDAL, IProductDAL productDAL)
        {
            CartDAL = purchaseDAL;
            ProductDAL = productDAL;
        }

        protected PurchaseItemDTO RecalculateFields(PurchaseItemDTO row)
        {  // Calculate total and apply discounts
            row.TotalPrice = row.Quantity * row.ItemPrice;
            return row;
        }

        public async Task<ShoppingCartDTO> GetShoppingCartAsync(int userID)
        {
            var purchasedItems = await CartDAL.GetAllPurchasedItemsAsync(userID);
            purchasedItems?.ForEach(x => RecalculateFields(x));

            var cart = new ShoppingCartDTO()
            {
                Items = purchasedItems ?? new List<PurchaseItemDTO>(),
                TotalPrice = purchasedItems?.Sum(x => x.TotalPrice) ?? 0
            };

            return cart;
        }

        public async Task<OperationResult> AddProductAsync(int userID, int productID, int quantity)
        {//Options:load full cart first and validate specials in cart/ add new specials
            OperationResult result = null;
            //simple validation that product really exists and we could sell it.
            //Is product in stock  or not is not an extremely important check
            //in some situation we may sell product even it out of stock but we have ability to get it in time
            //such logic it should be specified according to business requirements
            var isAvailable = await ProductDAL.IsProductInStockAsync(productID, quantity);
            if (!isAvailable)
            {
                var message = $"There are not enough product amount in stock. ProductID:{productID}";
                result = new OperationResult(message);
                return result;
            }
            //yes stock could be decreased between this and previous operation, we will re-check  before complete order

            var newItem = new PurchaseItemCreateDTO()
            {
                UserID = userID,
                ProductID = productID,
                Quantity = quantity
            };

            await CartDAL.BeginTransactionAsync();
            try
            {
                await CartDAL.AddProductToCartAsync(newItem);

                await ApplyProductSpecials(userID, productID);
                await CartDAL.CommitTransactionAsync();
                result = new OperationResult(true);
            }
            catch (Exception ex)
            {
                var message = $"Fail to add product with id:{productID} in stock.";
                result = new OperationResult(message);

                await CartDAL.RollbackTransactionAsync();
            }

            return result;
        }

        public async Task<OperationResult> DeleteProductAsync(int userID, int productID)
        {
            OperationResult result = null;
            await CartDAL.BeginTransactionAsync();
            try
            {
                var rowFound = await CartDAL.DeleteProductFromCartAsync(userID, productID);

                if (rowFound)
                {
                    await ApplyProductSpecials(userID, productID);
                }
                await CartDAL.CommitTransactionAsync();
                result = new OperationResult(true);
            }
            catch (Exception ex)
            {
                result = new OperationResult($"Failed delete product from cart. ProductId:{productID}");
                //option log exception
                await CartDAL.RollbackTransactionAsync();
            }

            return result;
        }

        public async Task<OperationResult> SetProductAmountAsync(int userID, int productID, int quantity)
        {
            OperationResult result = null;
            await CartDAL.BeginTransactionAsync();
            try
            {
                var rowFound = await CartDAL.SetProductQuantityAsync(userID, productID, quantity);

                if (!rowFound)
                {
                    var message = $"Unable to set product amount. Product with id={productID} is not in shopping cart.";
                    result = new OperationResult(message);
                }
                else
                {
                    await ApplyProductSpecials(userID, productID);
                    result = new OperationResult(true);
                };

                await CartDAL.CommitTransactionAsync();
            }
            catch (Exception ex)
            {
                result = new OperationResult("Failed change product amount in shopping cart. ProductID={productID}");
                await CartDAL.RollbackTransactionAsync();
            }

            return result;
        }

        /// <summary>
        /// Add actions and specials related to given product
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <param name="productID">Related product ID</param>
        protected async Task ApplyProductSpecials(int userID, int productID)
        {
            await CartDAL.DeleteProductBonusesAsync(userID, productID);

            var products = await CartDAL.GetProductPurchasedItemsAsync(userID, productID, false);
            var bonuses = await CartDAL.GetProductRelatedBonusesAsync(productID);

            var bonusProducts = new List<PurchaseBonusItemCreateDTO>();

            foreach (var bonus in bonuses)
            {
                int productQuantity = products.Where(x => x.IsSpecials == false
                                              && x.Item.ProductID == bonus.SourceProductId

                                                 )
                                                .Sum(x => x.Quantity);

                if (productQuantity >= bonus.MinQuantity)
                {
                    //How many times apply bonus to the same product
                    int bonusCount = (int)(productQuantity / bonus.MinQuantity);

                    bonusProducts.Add(new PurchaseBonusItemCreateDTO()
                    {
                        UserID = userID,
                        ProductID = bonus.BonusProductId,
                        SourceProductID = bonus.SourceProductId,
                        Quantity = bonus.BonusQuantity * bonusCount,
                        ItemPrice = bonus.BonusPrice,
                        IsSpecials = true
                    });
                }
            }

            await CartDAL.AddBonusesToCartAsync(bonusProducts);
        }
    }
}