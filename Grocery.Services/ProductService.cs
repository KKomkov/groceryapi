﻿using Grocery.Interfaces.DAL;
using Grocery.Interfaces.Services;
using Grocery.Models.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grocery.Services
{
    public class ProductService : IProductService
    {
        protected IProductDAL ProductDAL { get; }

        public ProductService(IProductDAL productDAL)
        {
            ProductDAL = productDAL;
        }

        public async Task<IEnumerable<ProductDTO>> GetAllProductsAsync(bool showOutOfStock = false)
        {
            return await ProductDAL.GetAllProductAsync(showOutOfStock);
        }

        public async Task<ProductDTO> GetProductAsync(int productID)
        {
            return await ProductDAL.GetProductAsync(productID);
        }
    }
}