﻿using AutoMapper;
using Grocery.Interfaces.DAL;
using Grocery.Interfaces.Services;
using Grocery.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grocery.Services
{
    public class OrderService : IOrderService
    {
        protected IPurchaseDAL CartDAL { get; }
        protected IOrderDAL OrderDAL { get; }

        protected IMapper _mapper;

        public OrderService(IPurchaseDAL cartDAL, IOrderDAL orderDAL, IMapper mapper)
        {
            CartDAL = cartDAL;
            OrderDAL = orderDAL;
            _mapper = mapper;
        }

        public async Task<CheckoutCartDTO> CheckoutCartAsync(int userId)
        {
            var result = new CheckoutCartDTO();
            var errorProductOutOfStock = "Not enough product items in a stock";
            try
            {
                var purchasedItems = await CartDAL.GetAllPurchasedItemsAsync(userId);
                if (purchasedItems.Count == 0)
                {
                    result.Errors = new List<ProductErrorDTO>()
                    {
                        new ProductErrorDTO()
                        {
                            ErrorMessage="Shopping cart is empty"
                        }
                    };
                    return result;
                }

                var itemsStock = await CartDAL.GetPurchasedItemsStockAsync(userId);

                var productOutOfStock = purchasedItems.GroupBy(x => x.Item.ProductID)
                             .Select(g => new
                             {
                                 ProductId = g.Key,
                                 Quantity = g.Sum(x => x.Quantity)
                             })
                             .Join(itemsStock,
                                   item => item.ProductId,
                                   stock => stock.ProductID,
                                   (item, stock) => new
                                   {
                                       productId = item.ProductId
                                                        ,
                                       purchasedQuantity = item.Quantity
                                                        ,
                                       inStockQuantity = stock.Quantity
                                   })
                             .Where(x => x.purchasedQuantity > x.inStockQuantity)
                             .Select(x => x.productId)
                             .ToList();

                if (productOutOfStock.Count > 0)
                {
                    result.Errors = productOutOfStock.Select(x => new ProductErrorDTO()
                    {
                        ProductID = x,
                        ErrorMessage = errorProductOutOfStock
                    })
                    .ToList();

                    return result;
                }

                //create Order
                result.NewOrder = await CreateOrderAsync(purchasedItems, userId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        protected async Task<OrderBaseDTO> CreateOrderAsync(List<PurchaseItemDTO> purchasedItems, int userId)
        {
            purchasedItems.ForEach(x => x.TotalPrice = x.Quantity * x.ItemPrice);

            OrderBaseDTO result;
            await OrderDAL.BeginTransactionAsync();
            try
            {
                var order = new OrderCreateDTO()
                {
                    PurchaseDate = DateTime.UtcNow,
                    UserId = userId,
                    Status = Models.BO.Enums.OrderStatus.Created
                };

                order.OrderItems = _mapper.Map<List<OrderItemCreateDTO>>(purchasedItems);
                order.PurchaseTotal = order.OrderItems.Sum(x => x.TotalPrice);

                result = await OrderDAL.CreateOrderAsync(order);
                await CartDAL.DeleteAllProductsFromCartAsync(userId);

                await OrderDAL.CommitTransactionAsync();
            }
            catch (Exception ex)
            {
                await OrderDAL.RollbackTransactionAsync();
                throw ex;
            }

            return result;
        }

        public async Task<IEnumerable<OrderDTO>> GetAllOrdersAsync(int userID)
        {
            return await OrderDAL.GetAllOrdersAsync(userID);
        }

        public async Task<OrderDTO> GetOrderAsync(int userID, int orderID)
        {
            return await OrderDAL.GetOrderAsync(userID, orderID);
        }
    }
}