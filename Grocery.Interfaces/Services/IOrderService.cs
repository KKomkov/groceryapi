﻿using Grocery.Models.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grocery.Interfaces.Services
{
    public interface IOrderService
    {
        Task<CheckoutCartDTO> CheckoutCartAsync(int userId);

        Task<IEnumerable<OrderDTO>> GetAllOrdersAsync(int userID);

        Task<OrderDTO> GetOrderAsync(int userID, int orderID);
    }
}