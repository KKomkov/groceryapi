﻿using Grocery.Models.DTO;
using System.Threading.Tasks;

namespace Grocery.Interfaces.Services
{
    public interface IPurchaseService
    {
        Task<ShoppingCartDTO> GetShoppingCartAsync(int UserID);

        Task<OperationResult> AddProductAsync(int userID, int productId, int quantity);

        Task<OperationResult> SetProductAmountAsync(int userID, int productId, int quantity);

        Task<OperationResult> DeleteProductAsync(int userID, int productId);
    }
}