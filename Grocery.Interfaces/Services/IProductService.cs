﻿using Grocery.Models.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grocery.Interfaces.Services
{
    public interface IProductService
    {
        Task<ProductDTO> GetProductAsync(int productID);

        Task<IEnumerable<ProductDTO>> GetAllProductsAsync(bool showOutOfStock = false);
    }
}