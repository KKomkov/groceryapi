﻿using Grocery.Models.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grocery.Interfaces.DAL
{
    public interface IProductDAL : IBaseDAL
    {
        Task<ProductDTO> GetProductAsync(int productId);

        Task<IEnumerable<ProductDTO>> GetAllProductAsync(bool showOutOfStock = false);

        Task<bool> IsProductInStockAsync(int productId, int quantity);
    }
}