﻿using System.Threading.Tasks;

namespace Grocery.Interfaces.DAL
{
    public interface IBaseDAL
    {
        Task BeginTransactionAsync();

        Task CommitTransactionAsync();

        Task RollbackTransactionAsync();
    }
}