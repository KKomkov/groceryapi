﻿using Grocery.Models.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grocery.Interfaces.DAL
{
    public interface IOrderDAL : IBaseDAL
    {
        Task<OrderBaseDTO> CreateOrderAsync(OrderCreateDTO newOrder);

        Task<IEnumerable<OrderDTO>> GetAllOrdersAsync(int userID);

        Task<OrderDTO> GetOrderAsync(int userID, int orderID);
    }
}