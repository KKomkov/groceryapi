﻿using Grocery.Models.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grocery.Interfaces.DAL
{
    public interface IPurchaseDAL : IBaseDAL
    {
        Task<List<PurchaseItemDTO>> GetAllPurchasedItemsAsync(int userID);

        Task<List<ProductQuantityDTO>> GetPurchasedItemsStockAsync(int userID);

        Task<List<int>> GetPurchasedItemsOutOfStockAsync(int userID);

        Task AddProductToCartAsync(PurchaseItemCreateDTO item);

        /// <summary>
        /// Try delete product from cart
        /// </summary>
        /// <param name="userID">User Id</param>
        /// <param name="productID">Product ID</param>
        /// <returns>Weather is product found in cart before delete.
        /// If product is not in a cart then result false.
        /// </returns>
        Task<bool> DeleteProductFromCartAsync(int userID, int productID);

        Task DeleteAllProductsFromCartAsync(int userID);

        /// <summary>
        /// Set quantity of a product in cart
        /// </summary>
        /// <param name="userID">User Id</param>
        /// <param name="productID">Product ID</param>
        /// <param name="quantity">Desirable product quantity</param>
        /// <returns>Weather is product found in cart.
        /// If product is not in a cart then result false.
        /// </returns>
        Task<bool> SetProductQuantityAsync(int userID, int productID, int quantity);

        Task<List<PurchaseItemDTO>> GetProductPurchasedItemsAsync(int userId, int productID, bool includeSpecials = false);

        #region Bonuses

        Task<List<BonusDTO>> GetProductRelatedBonusesAsync(int productID);

        Task AddBonusesToCartAsync(IEnumerable<PurchaseBonusItemCreateDTO> bonusItems);

        Task DeleteProductBonusesAsync(int userID, int productID);

        #endregion Bonuses
    }
}